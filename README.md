## **Humans Need Not Apply**

---
"Humanos Não São Desejados" foi o documentario escolhido por nós para realizar o trabalho de Introdução à Engenharia. 

Ele está disponível, gratuitamente, no YouTube, com legendas em Português(Brasil), no seguinte link:

[Humans Need Not Apply](https://youtu.be/7Pq-S557XQU)

## Autoras

| Avatar | Nome | Nickname | Email |
| ------ | ---- | -------- | ----- |
| ![](https://gitlab.com/uploads/-/system/user/avatar/11198143/avatar.png?width=400)  | Caroline Morelli da Silveira | Carweni | [caroline_ms2004@hotmail.com](mailto:caroline_ms2004@hotmail.com)
| ![](https://gitlab.com/uploads/-/system/user/avatar/11206926/avatar.png?width=400)  | Laura Armiliato Sangalli| Laura-Sangalli | [lauraasangalli@gmail.com](mailto:lauraasangalli@gmail.com)
| ![](https://gitlab.com/uploads/-/system/user/avatar/11198315/avatar.png?width=400)  | Maria Cristina Fabiane | MacriFabiane | [mariacristinafabiane@gmail.com](mailto:mariacristinafabiane@gmail.com)

